

# sisop-praktikum-fp-2023-ws-f08

# Kelompok F08 :
| Nama | NRP |
| ---------------------- | ---------- |
| Faiz Haq Noviandra | 5025211132 |
| Nayya Kamila Putri Y | 5025211183 |
| Cavel Ferrari | 5025211198 |

### Autentikasi

Bagian ini digunakan untuk menciptakan pengguna dan mengatur izin akses pengguna agar dapat memasuki basis data.
```
int check_permission(char *uname, char *pass)
{
    FILE *file;
    char lines[128];
    struct permission client;

    file = fopen("../database/databases/user.txt", "r");
    if (file == NULL)
    {
        printf("Failed to open file\n");
        return 0;
    }

    while (fgets(lines, sizeof(lines), file) != NULL)
    {
        sscanf(lines, "Username: %[^,], Password: %[^;]", client.name, client.pass);

        int nameComparison = strcmp(client.name, uname);
        int passComparison = strcmp(client.pass, pass);

        if (nameComparison == 0 && passComparison == 0)
        {
            fclose(file);
            return 1;
        }
    }

    fclose(file);
    return 0;
}
```
Fungsi check_permission digunakan untuk melakukan verifikasi apakah terdapat pengguna dengan nama pengguna tertentu dalam basis data atau tidak. Konsepnya adalah apabila pengguna ditemukan, maka file basis data akan ditutup dan fungsi akan mengembalikan nilai 1, menandakan bahwa pengguna telah terdaftar. Namun, jika setelah memeriksa seluruh baris dalam file tidak ada kecocokan dengan nama pengguna yang diberikan, maka file basis data juga ditutup dan fungsi akan mengembalikan nilai 0, yang menunjukkan bahwa pengguna tidak ditemukan dalam basis data.
```
void create_user(char *name, char *pass)
{
    FILE *file;
    file = fopen("databases/user.txt", "a");

    if (file == NULL)
    {
        printf("Error opening file!\n");
        return;
    }

    fprintf(file, "\nUsername: %s, Password: %s", name, pass;
    fclose(file);
}
```
Fungsi ini digunakan untuk membuat entri baru dalam file basis data pengguna. Fungsi ini menerima dua argumen, yaitu name (nama pengguna baru) dan pass (kata sandi pengguna baru). Jika file berhasil dibuka, fungsi menggunakan fungsi fprintf untuk menulis entri baru ke dalam file basis data pengguna. Format entri tersebut adalah "Username: %s, Password: %s", di mana %s adalah placeholder yang akan digantikan dengan nilai dari name dan pass. Setelah menulis entri baru, file ditutup menggunakan fungsi fclose untuk memastikan perubahan disimpan. Kode ini mengasumsikan bahwa file basis data pengguna ("databases/user.txt") berada dalam direktori yang sama dengan kode yang sedang dieksekusi. Jika file tidak ditemukan, pesan kesalahan akan dicetak.

### Autorisasi
Pada bagian ini, dilakukan pengaturan izin (permission) untuk setiap pengguna. Izin ini hanya dapat diatur oleh pengguna dengan hak akses root.
```
int is_allowed_db(char *name, char *database)
    {
        FILE *file;
        struct permission_db user;
        int mark = 0;
        if (strlen(database) > 0 && database[strlen(database) - 1] == ';')
        {
            database[strlen(database) - 1] = '\0';
        }
        printf("name = %s  database = %s\n", name, database);
        file = fopen("databases/permission.txt", "r"); // Open the file in read mode
        if (file == NULL)
        {
            printf("[-] Error in opening file.\n");
            return 0;
        }
        while (fscanf(file, "%s %s", user.name, user.database) != EOF)
        {
            int nameComparison = strcmp(user.name, name);
            int passComparison = strcmp(user.database, database);

            printf("Name comparison result: %d\n", nameComparison);
            printf("Password comparison result: %d\n", passComparison);

            if (strcmp(user.name, name) == 0 && strcmp(user.database, database) == 0)
            {
                mark = 1;
                break;
            }
        }
        fclose(file);
        return mark;
    }
```
Fungsi is_allowed_db digunakan untuk memeriksa apakah pengguna memiliki izin untuk mengakses database tertentu. Fungsi ini menerima dua argumen, yaitu name (nama pengguna) dan database (nama database).
Pada awalnya, fungsi ini melakukan pemeriksaan pada database untuk memastikan bahwa tidak ada karakter ';' di akhir string. Jika ada, karakter ';' dihapus dengan menggantinya dengan '\0'. Hal ini dilakukan untuk memastikan kesesuaian saat membandingkan string.
Selanjutnya, fungsi membuka file "permission.txt" yang berisi daftar izin pengguna untuk mengakses database. File tersebut dibuka dalam mode "r" (baca) menggunakan fungsi fopen. Setelah membuka file, fungsi memeriksa apakah file berhasil dibuka atau tidak dengan memeriksa apakah variabel file memiliki nilai NULL. Jika file tidak dapat dibuka, pesan kesalahan akan dicetak dan fungsi akan keluar.
Selanjutnya, fungsi melakukan perulangan untuk memeriksa setiap baris dalam file. Jika ada kecocokan antara name dan user.name, serta antara database dan user.database, variabel mark diubah menjadi 1 dan perulangan dihentikan menggunakan break.
Akhirnya, fungsi mengembalikan nilai mark, yang akan menjadi 1 jika ada izin yang sesuai, dan 0 jika tidak ada izin yang sesuai.
```
void write_permission(char *name, char *database)
    {
        struct permission_db user;

        strcpy(user.name, name);
        strcpy(user.database, database);

        printf("%s %s\n", user.name, user.database);

        char fname[] = {"databases/permission.txt"};
        FILE *file;
        file = fopen(fname, "a");
        fprintf(file, "\n%s %s", user.name, user.database);
        fclose(file);
    }
```
Fungsi write_permission digunakan untuk menulis izin akses pengguna ke dalam file "permission.txt". Fungsi ini menerima dua argumen, yaitu name (nama pengguna) dan database (nama database).
Pertama, sebuah variabel user dari tipe struct permission_db dideklarasikan. Struktur ini memiliki dua anggota, yaitu name dan database, yang digunakan untuk menyimpan nilai name dan database yang diberikan.
Selanjutnya, file "permission.txt" dibuka dalam mode "a" (append) menggunakan fungsi fopen. Setelah file dibuka, fungsi menggunakan fprintf untuk menulis nilai name dan database ke dalam file, diikuti dengan karakter baris baru. Data ditulis dalam format "%s %s", yang memisahkan name dan database dengan spasi.
Dengan demikian, fungsi write_permission digunakan untuk menambahkan izin akses pengguna ke dalam file "permission.txt" dengan format yang sesuai, di mana setiap baris dalam file mewakili izin akses satu pengguna untuk satu database.
### Data Definition Language
Di bagian ini, terdapat beberapa pernyataan yang digunakan sebagai bahasa definisi data (DDL) dalam database. Berikut ini beberapa contohnya:
```
else if (strcmp(command[0], "CREATEDATABASE") == 0)
    {
        char location[2048];
        if (strlen(command[1]) > 0 && command[1][strlen(command[1]) - 1] == ';')
        {
            command[1][strlen(command[1]) - 1] = '\0';
        }
        snprintf(location, sizeof location, "databases/%s", command[1]);
        printf("location = %s, name = %s , database = %s\n", location, command[2], command[1]);
        mkdir(location, 0777);
        write_permission(command[2], command[1]);
    }
```
Blok kode ini merupakan bagian dari sebuah blok "else if" dalam suatu program yang melakukan pengecekan apakah perintah yang diberikan adalah "CREATEDATABASE". Jika perintah tersebut cocok, maka akan dilakukan beberapa tindakan sesuai dengan perintah-perintah di bawahnya.
```
else if (strcmp(command[0], "CREATETABLE") == 0)
    {
        printf("%s\n", command[1]);
        char *currToken;

    if (currDB[0] == '\0')
        {
            strcpy(currDB, "Select a Database First");
            send(newSocket, currDB, strlen(currDB), 0);
            str_empty(buffer, sizeof(buffer));
        }
        else
        {
            char query_list[128][1024];
            char temp_cmd[2048];
            snprintf(temp_cmd, sizeof temp_cmd, "%s", command[1]);
            currToken = strtok(temp_cmd, "(), ");
            int total = 0;

            while (currToken != NULL)
            {
                strcpy(query_list[total], currToken);
                printf("%s\n", query_list[total]);
                total++;
                currToken = strtok(NULL, "(), ");
            }

            if (strlen(currDB) > 0 && currDB[strlen(currDB) - 1] == ';')
            {
                currDB[strlen(currDB) - 1] = '\0';
            }

            char createTable[2048];
            if (strlen(query_list[2]) > 0 && query_list[2][strlen(query_list[2]) - 1] == ';')
            {
                query_list[2][strlen(query_list[2]) - 1] = '\0';
            }
            snprintf(createTable, sizeof createTable, "../database/databases/%s/%s", currDB, query_list[2]);
            int iteration = 0;
            int data_iteration = 3;
            struct table column;

            while (total > 3)
            {
                strcpy(column.data[iteration], query_list[data_iteration]);
                printf("%s\n", column.data[iteration]);
                strcpy(column.type[iteration], query_list[data_iteration + 1]);
                data_iteration = data_iteration + 2;
                total = total - 2;
                iteration++;
            }

            column.totalAttr = iteration;
            FILE *file;

            printf("%s\n", createTable);
            file = fopen(createTable, "w"); // Open the file in write mode
            if (file == NULL)
            {
                printf("SERVER : Error while opening file.\n");
                return 0;
            }
            iteration--;
            fprintf(file, "Total Columns: %d\n", iteration--);
            for (int i = 0; i <= iteration; i++)
            {
                fprintf(file, "Data: %s, Type: %s\n", column.data[i], column.type[i]);
            }

            fclose(file);
        }
    }
```
Blok kode ini merupakan bagian dari sebuah blok "else if" dalam suatu program yang melakukan pengecekan apakah perintah yang diberikan adalah "CREATETABLE". Jika perintah tersebut cocok, maka akan dilakukan beberapa tindakan sesuai dengan perintah-perintah yang terdapat di bawahnya.
```
else if (strcmp(command[0], "DROPDATABASE") == 0)
    {
        int userAllowed = is_allowed_db(command[2], command[1]);

        if (userAllowed != 1)
        {
            char message[] = "Request Denied!";
            send(newSocket, message, strlen(message), 0);
            str_empty(buffer, sizeof(buffer));
            continue;
        }
        else
        {
            char delete[2048];
            snprintf(delete, sizeof delete, "rm -r databases/%s", command[1]);
            system(delete);
            char message[] = "Database Has Been Removed";
            send(newSocket, message, strlen(message), 0);
            str_empty(buffer, sizeof(buffer));
        }
    }
```
Blok kode ini merupakan bagian dari sebuah blok "else if" dalam suatu program yang melakukan pengecekan apakah perintah yang diberikan adalah "DROPDATABASE". Jika perintah tersebut cocok, maka akan dilakukan beberapa tindakan sesuai dengan perintah-perintah yang terdapat di bawahnya.
```
else if (strcmp(command[0], "DROPTABLE") == 0)
    {
        if (currDB[0] == '\0')
        {
            strcpy(currDB, "Select a Database First");
            send(newSocket, currDB, strlen(currDB), 0);
            str_empty(buffer, sizeof(buffer));
            continue;
        }

        char delete[2048];
        if (strlen(command[1]) > 0)
        {
            command[1][strlen(command[1]) - 1] = '\0';
        }
        snprintf(delete, sizeof delete, "databases/%s/%s", currDB, command[1]);
        remove(delete);
        char message[] = "Table Has Been Removed";
        send(newSocket, message, strlen(message), 0);
        str_empty(buffer, sizeof(buffer));
    }
```
Blok kode ini merupakan bagian dari sebuah blok "else if" dalam suatu program yang melakukan pengecekan apakah perintah yang diberikan adalah "DROPTABLE". Jika perintah tersebut cocok, maka akan dilakukan beberapa tindakan sesuai dengan perintah-perintah yang terdapat di bawahnya.
```else if (strcmp(command[0], "DROPCOLUMN") == 0)
    {
        if (currDB[0] == '\0')
        {
            strcpy(currDB, "Select a Database First");
            send(newSocket, currDB, strlen(currDB), 0);
            str_empty(buffer, sizeof(buffer));
            continue;
        }

        if (strlen(command[2]) > 0 && command[2][strlen(command[2]) - 1] == ';')
        {
            command[2][strlen(command[2]) - 1] = '\0';
        }
        if (strlen(currDB) > 0 && currDB[strlen(currDB) - 1] == ';')
        {
            currDB[strlen(currDB) - 1] = '\0';
        }

        char createTable[2048];
        snprintf(createTable, sizeof createTable, "databases/%s/%s", currDB, command[2]);
        printf("1 : %s\n", command[1]);
        printf("2 : %s\n", command[2]);
        int index = find_column(createTable, command[1]);

        if (index == -1)
        {
            char message[] = "Column Not Found";
            send(newSocket, message, strlen(message), 0);
            str_empty(buffer, sizeof(buffer));
            continue;
        }

        drop_attribute(createTable, index);
        char message[] = "Column Has Been Removed";
        send(newSocket, message, strlen(message), 0);
        str_empty(buffer, sizeof(buffer));
    }```
Blok kode ini merupakan bagian dari sebuah blok "else if" dalam suatu program yang melakukan pengecekan apakah perintah yang diberikan adalah "DROPCOLUMN". Jika perintah tersebut cocok, maka akan dilakukan beberapa tindakan sesuai dengan perintah-perintah yang terdapat di bawahnya.
### Data Manipulation Language
Pada bagian ini terdapat beberapa syntax yang dibuat sebagai DML dalam database. seperti berikut:
```
else if (strcmp(command[0], "INSERT") == 0)
    {
        if (currDB[0] == '\0')
        {
            strcpy(currDB, "Select a Database First");
            send(newSocket, currDB, strlen(currDB), 0);
            str_empty(buffer, sizeof(buffer));
            continue;
        }
    }
```
Blok kode ini merupakan bagian dari sebuah blok "else if" dalam suatu program yang melakukan pengecekan apakah perintah yang diberikan adalah "INSERT". Jika perintah tersebut cocok, maka akan dilakukan beberapa tindakan sesuai dengan perintah-perintah yang terdapat di bawahnya.
```
else if (strcmp(command[0], "UPDATE") == 0)
    {
        if (currDB[0] == '\0')
        {
            strcpy(currDB, "Select a Database First");
            send(newSocket, currDB, strlen(currDB), 0);
            str_empty(buffer, sizeof(buffer));
            continue;
        }
    }
```
Blok kode ini merupakan bagian dari sebuah blok "else if" dalam suatu program yang melakukan pengecekan apakah perintah yang diberikan adalah "UPDATE". Jika perintah tersebut cocok, maka akan dilakukan beberapa tindakan sesuai dengan perintah-perintah yang terdapat di bawahnya.
```
else if (strcmp(command[0], "DELETE") == 0)
    {
        if (currDB[0] == '\0')
        {
            strcpy(currDB, "Select a Database First");
            send(newSocket, currDB, strlen(currDB), 0);
            str_empty(buffer, sizeof(buffer));
            continue;
        }
    }
```
Blok kode ini merupakan bagian dari sebuah blok "else if" dalam suatu program yang melakukan pengecekan apakah perintah yang diberikan adalah "DELETE". Jika perintah tersebut cocok, maka akan dilakukan beberapa tindakan sesuai dengan perintah-perintah yang terdapat di bawahnya.
```
else if (strcmp(command[0], "SELECT") == 0)
    {
        if (currDB[0] == '\0')
        {
            strcpy(currDB, "Select a Database First");
            send(newSocket, currDB, strlen(currDB), 0);
            str_empty(buffer, sizeof(buffer));
            continue;
        }
    }
```
Blok kode ini merupakan bagian dari sebuah blok "else if" dalam suatu program yang melakukan pengecekan apakah perintah yang diberikan adalah "SELECT". Jika perintah tersebut cocok, maka akan dilakukan beberapa tindakan sesuai dengan perintah-perintah yang terdapat di bawahnya.
### Logging
Setiap command yang dipakai harus dilakukan logging ke suatu file dengan format yang sudah ditentukan. Contohnya  jika yang eksekusi root, maka username akan menampilkan root
```
void append_log(char *command, char *name)
    {
        time_t times;
        struct tm *info;
        time(&times);
        info = localtime(&times);

        char write_log[256];

        FILE *file;
        char location[256];
        snprintf(location, sizeof location, "../database/log/log.log", name);
        file = fopen(location, "a");

        sprintf(write_log, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n",
                info->tm_year + 1900, info->tm_mon + 1, info->tm_mday, info->tm_hour, info->tm_min, info->tm_sec, name, command);

        fputs(write_log, file);
        fclose(file);
    }
```
Fungsi append_log ini berfungsi untuk menambahkan log ke file log.log yang terletak di dalam direktori log. Pertama, fungsi ini menggunakan variabel times dengan tipe data time_t dan info dengan tipe data struct tm untuk mendapatkan informasi waktu saat ini menggunakan fungsi time(). Selanjutnya, fungsi membuka file log.log dengan mode "a" (append) menggunakan fungsi fopen. Lokasi file log.log ditentukan oleh variabel location, yang dibentuk menggunakan fungsi snprintf dengan format "../database/log/log.log". Setelah itu, fungsi menggunakan sprintf untuk memformat teks log yang akan ditulis ke file. Format teks log ini mencakup informasi waktu (tahun, bulan, hari, jam, menit, detik), diikuti oleh nama pengguna (name) dan perintah (command). Informasi waktu diambil dari struktur info dengan mengakses elemen-elemen seperti tm_year, tm_mon, tm_mday, tm_hour, tm_min, dan tm_sec. Setelah teks log diformat, fungsi menggunakan fputs untuk menulis teks log ke file log.log yang telah dibuka sebelumnya.
Dengan demikian, saat fungsi append_log dipanggil, log yang berisi informasi waktu, nama pengguna, dan perintah akan ditambahkan ke file log.log di dalam direktori log.
### Tambahan
Melakukan Redirection melalui sebuah file
```
else if (strcmp(argv[i], "-d") == 0)
        {
            database = argv[i + 1];
        }
```
Membaca jika ada -d saat melakukan run program_client
```
if (database != NULL)
    {
        printf("Entering database %s\n", database);
        snprintf(buffer, sizeof buffer, "USEDATABASE:%s:%s:%d", database, argv[2], idUser);
        send(clientSocket, buffer, strlen(buffer), 0);
    }
```



